package drug

import "database/sql"

type Drug struct {
	ID           int
	Name_pill    string
	Pill_content string
}

func (p *Drug) Insert(db *sql.DB) {
	db.QueryRow("INSERT INTO drug(name_pill, pill_content) VALUES($1, $2) returning id;", p.Name_pill, p.Pill_content).Scan(&p.ID)
}

func (ph *Drug) CreateTable(db *sql.DB) error {
	_, err := db.Exec(`CREATE TABLE drug(id BIGSERIAL PRIMARY KEY, name_pill VARCHAR(50) NOT NULL, pill_content VARCHAR(100) NOT NULL);`)
	return err
}

func (ph *Drug) SelectAll(db *sql.DB) (*sql.Rows, error) {
	return db.Query("SELECT * FROM drug;")
}

func (ph *Drug) UpdateName(db *sql.DB, newName string, id int) error {
	ph.Name_pill = newName
	_, err := db.Exec("UPDATE drug SET name_pill=$1 where id=$2", ph.Name_pill, id)
	return err
}

func (ph *Drug) Delete(db *sql.DB, id int) error {
	_, err := db.Exec("DELETE FROM drug where id=$1", id)
	return err
}
