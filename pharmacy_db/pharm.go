package pharmacydb

import (
	"database/sql"
)

type Pharm struct {
	ID         int
	Name_pharm string
	Company    string
}

func (ph *Pharm) Insert(db *sql.DB) {
	db.QueryRow("INSERT INTO pharmacies(name_pharm, company) VALUES($1, $2) returning id;", ph.Name_pharm, ph.Company).Scan(&ph.ID)
}

func (ph *Pharm) CreateTable(db *sql.DB)error{
	_, err := db.Exec(`CREATE TABLE pharmacies(id BIGSERIAL PRIMARY KEY, name_pharm VARCHAR(50) NOT NULL, company VARCHAR(100) NOT NULL);`)
	return err
}

func (ph *Pharm) SelectAll(db *sql.DB) (*sql.Rows, error) {
	return db.Query("SELECT * FROM pharmacies;")
}

func (ph *Pharm) UpdateName(db *sql.DB, newName string, id int) error {
	ph.Name_pharm = newName
	_, err := db.Exec("UPDATE pharmacies SET name_pharm=$1 where id=$2", newName, id)
	return err
}

func (ph *Pharm) Delete(db *sql.DB, id int) error {
	_, err := db.Exec("DELETE FROM pharmacies where id=$1", id)
	return err
}