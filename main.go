package main

import (
	dbconnections "Projects/go-postgresql/db_connections"
	"Projects/go-postgresql/drug"
	pharmaciesdata "Projects/go-postgresql/pharmacies_data"
	pharmacydb "Projects/go-postgresql/pharmacy_db"
	"bufio"
	"fmt"
	"os"
	"strconv"

	_ "github.com/lib/pq"
)

type Responce struct {
	Pill_name    string
	Pill_content string
	Pharm_name   string
	Price        float64
	Amount       int
}

func main() {
	s := bufio.NewScanner(os.Stdin)

	db := dbconnections.GetDb()
	defer db.Close()
	d := &drug.Drug{}
	p := &pharmacydb.Pharm{}
	ph := &pharmaciesdata.Pharm_data{}
	choose := "0"
	for {
		Menu()
		fmt.Scan(&choose)
		if choose == "1" {
			for {
				drugsMenu()
				fmt.Scan(&choose)
				if choose == "6" {
					break
				}
				switch choose {
				case "1":
					d.CreateTable(db)
				case "2":
					fmt.Print("Enter name of the pill: ")
					s.Scan()
					d.Name_pill = s.Text()
					fmt.Print("Enter content of the pill: ")
					s.Scan()
					d.Pill_content = s.Text()
					d.Insert(db)
				case "3":
					var temp_id int
					fmt.Print("Enter id of pil that you want to delete: ")
					fmt.Scan(&temp_id)
					d.Delete(db, temp_id)
				case "4":
					rows, err := d.SelectAll(db)
					if err != nil {
						fmt.Println(err)
					}
					for rows.Next() {
						temp := drug.Drug{}
						err := rows.Scan(&temp.ID, &temp.Name_pill, &temp.Pill_content)
						if err != nil {
							fmt.Println(err)
						}
						fmt.Println(temp)
					}
					rows.Close()
				case "5":
					var temp_id int
					fmt.Print("Enter id of pil that you want to update: ")
					fmt.Scan(&temp_id)
					var newName string
					fmt.Print("Enter new name: ")
					fmt.Scan(&newName)

					if err := d.UpdateName(db, newName, temp_id); err != nil {
						fmt.Println(err)
					}
				}
			}
		} else if choose == "2" {
			for {
				pharm_Menu()
				fmt.Scan(&choose)
				if choose == "6" {
					break
				}
				switch choose {
				case "1":
					if err := p.CreateTable(db); err != nil {
						fmt.Println(err)
					}
				case "2":
					fmt.Print("Enter name of the pharmacy: ")
					s.Scan()
					p.Name_pharm = s.Text()
					fmt.Print("Etner name of the owner company: ")
					s.Scan()
					p.Company = s.Text()
					p.Insert(db)
				case "3":
					var temp_id int
					fmt.Print("Enter id of pharm that you want to delete: ")
					fmt.Scan(&temp_id)
					if err := p.Delete(db, temp_id); err != nil {
						fmt.Println(err)
					}

				case "4":
					rows2, err := p.SelectAll(db)
					if err != nil {
						fmt.Println(err)
					}
					for rows2.Next() {
						temp := pharmacydb.Pharm{}
						err := rows2.Scan(&temp.ID, &temp.Name_pharm, &temp.Company)
						if err != nil {
							fmt.Println(err)
						}
						fmt.Println(temp)
					}
					rows2.Close()
				case "5":
					var temp_id int
					fmt.Print("Enter id of pil that you want to update: ")
					fmt.Scan(&temp_id)
					var newName string
					fmt.Print("Enter new name: ")
					fmt.Scan(&newName)

					if err := p.UpdateName(db, newName, temp_id); err != nil {
						fmt.Println(err)
					}
				}
			}

		} else if choose == "3" {
			for {
				pharm_data_menu()
				fmt.Scan(&choose)
				if choose == "5" {
					break
				}
				switch choose {
				case "1":
					ph.CreateTable(db)
				case "2":
					fmt.Print("Pharm_id: ")
					s.Scan()
					temp, err := strconv.Atoi(s.Text())
					if err != nil {
						fmt.Println(err)
					}
					ph.Pharm_id = temp
					fmt.Print("Pill_id: ")
					s.Scan()
					temp, err = strconv.Atoi(s.Text())
					if err != nil {
						fmt.Println(err)
					}
					ph.Pill_id = temp
					fmt.Print("Price: ")
					if _, err := fmt.Scan(&ph.Price); err != nil {
						fmt.Println(err)
					}
					fmt.Print("Amount: ")
					if _, err := fmt.Scan(&ph.Amount); err != nil {
						fmt.Println(err)
					}
					ph.Insert(db)
				case "3":
					rows3, err := ph.SelectAll(db)
					if err != nil {
						fmt.Print(err)
					}
					for rows3.Next() {
						if err := rows3.Scan(ph.Pharm_id, ph.Pill_id, ph.Price, ph.Amount); err != nil {
							fmt.Print(err)
						}

					}
					rows3.Close()
				case "4":
					rows4, err := ph.SelectAllJoining(db)
					if err != nil {
						fmt.Println(err)
					}
					var pharm_data_obj Responce
					for rows4.Next() {
						if err := rows4.Scan(&pharm_data_obj.Pill_name, &pharm_data_obj.Pill_content, &pharm_data_obj.Pharm_name, &pharm_data_obj.Price, &pharm_data_obj.Amount); err != nil {
							fmt.Println(err)
						}
						fmt.Println(pharm_data_obj)
					}
					rows4.Close()
				}
			}
		} else {
			break
		}
	}

}

func Menu() {
	fmt.Println("       Main Menu ")
	fmt.Println(" 1. drugs")
	fmt.Println(" 2. pharms")
	fmt.Println(" 3. pharm_data")
	fmt.Println(" 4. exit")
}

func drugsMenu() {
	fmt.Println("		Drugs")
	fmt.Println(" 1. Create table")
	fmt.Println(" 2. Insert")
	fmt.Println(" 3. Delete by id")
	fmt.Println(" 4. Select all")
	fmt.Println(" 5. Update name")
	fmt.Println(" 6. Exit")
}

func pharm_Menu() {
	fmt.Println("		Pharmacies")
	fmt.Println(" 1. Create table")
	fmt.Println(" 2. Insert")
	fmt.Println(" 3. Delete by id")
	fmt.Println(" 4. Select all")
	fmt.Println(" 5. Update name")
	fmt.Println(" 6. Exit")
}

func pharm_data_menu() {
	fmt.Println("		Pharmacies data")
	fmt.Println(" 1. Create table")
	fmt.Println(" 2. Insert")
	fmt.Println(" 3. Select all")
	fmt.Println(" 4. Select JOINING pharm and pill tables")
	fmt.Println(" >=5. Exit")
}
