package pharmaciesdata

import "database/sql"

type Pharm_data struct {
	Pharm_id int
	Pill_id  int
	Price    float64
	Amount   int
}

func (ph *Pharm_data) Insert(db *sql.DB) {
	db.QueryRow("INSERT INTO pharm_data(pharm_id, pill_id, price, amount) VALUES($1, $2, $3, $4);", ph.Pharm_id, ph.Pill_id, ph.Price, ph.Amount)
}

func (ph *Pharm_data) CreateTable(db *sql.DB) error {
	_, err := db.Exec(`CREATE TABLE pharm_data(pharm_id int REFERENCES pharmacies(id),
	pill_id int REFERENCES drug(id), price decimal NOT NULL, amount INT NOT NULL);`)
	return err
}

func (ph *Pharm_data) SelectAll(db *sql.DB) (*sql.Rows, error) {
	return db.Query("SELECT * FROM pharm_data;")
}

func (ph *Pharm_data) UpdateAmount(db *sql.DB, newAmount string, id int) error {
	_, err := db.Exec("UPDATE pharm_data SET amount=$1 where id=$2", newAmount, id)
	return err
}
func (ph *Pharm_data) SelectAllJoining(db *sql.DB) (*sql.Rows, error) {
	return db.Query(`
	SELECT name_pill, pill_content, name_pharm, price, amount FROM drug d 
	inner join pharm_data ph ON ph.pill_id = d.id 
	inner join pharmacies p ON ph.pharm_id = p.id
	`)
}

// func (ph *Pharm_data) Delete(db *sql.DB, id int) error {
// 	_, err := db.Exec("DELETE FROM pharm_data where id=$1", id)
// 	return err
// }
